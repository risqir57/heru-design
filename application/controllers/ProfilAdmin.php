<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ProfilAdmin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('ProfilAdminModel');
        $this->load->library('form_validation');
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('profiladmin/profil_tbl_list');
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->ProfilAdminModel->json();
    }

    public function read($id)
    {
        $row = $this->ProfilAdminModel->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'images' => $row->images,
		'tittle' => $row->tittle,
		'posisi' => $row->posisi,
		'status' => $row->status,
	    );
            $this->load->view('profiladmin/profil_tbl_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('profiladmin'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('profiladmin/create_action'),
	    'id' => set_value('id'),
	    'images' => set_value('images'),
	    'tittle' => set_value('tittle'),
	    'posisi' => set_value('posisi'),
	    'status' => set_value('status'),
	);
        $this->load->view('profiladmin/profil_tbl_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
          $config['upload_path'] = './assets/images';
          $config['allowed_types'] = 'gif|png|jpg|jpeg';
          $config['encrypt_name'] = TRUE;
          $this->upload->initialize($config);
          $this->upload->do_upload('images');
          $img = $this->upload->data();
          $images = $img['file_name'];
          $data = array(
            'images' => $images,
            'tittle' => $this->input->post('tittle',TRUE),
            'status' => $this->input->post('status',TRUE),
            'posisi' => $this->input->post('posisi',TRUE),
          );

            $this->ProfilAdminModel->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('profiladmin'));
        }
    }

    public function update($id)
    {
        $row = $this->ProfilAdminModel->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('profiladmin/update_action'),
		'id' => set_value('id', $row->id),
		'images' => set_value('images', $row->images),
		'tittle' => set_value('tittle', $row->tittle),
		'posisi' => set_value('posisi', $row->posisi),
		'status' => set_value('status', $row->status),
	    );
            $this->load->view('profiladmin/profil_tbl_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('profiladmin'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
          if (empty($_FILES['images']['name'])) {
            $data = array(
              'tittle' => $this->input->post('tittle',TRUE),
              'status' => $this->input->post('status',TRUE),
              'posisi' => $this->input->post('posisi',TRUE),
            );
          } else {
            $config['upload_path'] = './assets/images';
            $config['allowed_types'] = 'gif|png|jpg|jpeg';
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);
            $this->upload->do_upload('images');
            $img = $this->upload->data();
            $images = $img['file_name'];
            $data = array(
              'images' => $images,
              'tittle' => $this->input->post('tittle',TRUE),
              'status' => $this->input->post('status',TRUE),
              'posisi' => $this->input->post('posisi',TRUE),
            );
          }
          $this->ProfilAdminModel->update($this->input->post('id', TRUE), $data);
          $this->session->set_flashdata('message', 'Update Record Success');
          redirect(site_url('profiladmin'));
        }
    }

    public function delete($id)
    {
        $row = $this->ProfilAdminModel->get_by_id($id);

        if ($row) {
            $this->ProfilAdminModel->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('profiladmin'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('profiladmin'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('tittle', 'tittle', 'trim|required');
	$this->form_validation->set_rules('posisi', 'posisi', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file ProfilAdmin.php */
/* Location: ./application/controllers/ProfilAdmin.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-09-22 07:31:17 */
/* http://harviacode.com */
