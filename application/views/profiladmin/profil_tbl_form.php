<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Profil_tbl <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" accept-charset="utf8">
    	    <div class="form-group">
                <label for="varchar">Images <?php echo form_error('images') ?></label>
                <input type="file" class="form-control" name="images" id="images" placeholder="Images" value="<?php echo $images; ?>" />
            </div>
    	    <div class="form-group">
                <label for="varchar">Tittle <?php echo form_error('tittle') ?></label>
                <input type="text" class="form-control" name="tittle" id="tittle" placeholder="Tittle" value="<?php echo $tittle; ?>" />
            </div>
    	    <div class="form-group">
                <label for="int">Posisi <?php echo form_error('posisi') ?></label>
                <input type="text" class="form-control" name="posisi" id="posisi" placeholder="Posisi" value="<?php echo $posisi; ?>" />
            </div>
    	    <div class="form-group">
                <label for="tinyint">Status <?php echo form_error('status') ?></label>
                <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" />
            </div>
    	    <input type="hidden" name="id" value="<?php echo $id; ?>" />
    	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
    	    <a href="<?php echo site_url('profiladmin') ?>" class="btn btn-default">Cancel</a>
    	</form>
    </body>
</html>
